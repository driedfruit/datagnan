# Datagnan #

[gem_link]: https://rubygems.org/gems/datagnan "gem install datagnan"
[![gem install datagnan](https://badge.fury.io/rb/datagnan.svg)][gem_link]

HTML-templates engine, without specific (often ugly) tags. Based on `data-` attributes.

------------------
## Installation ##

* `gem install datagnan`
* Include gem in your project: `require 'datagnan'`

---------------------

## Helper function ##

This function adapted for [Sinatra](http://www.sinatrarb.com/). You can write your own using the class `Datagnan`.

`datagnan(template_file, options = {}, locals = {})`

* __template_file__ _(String or Symbol)_ - name of the required .html-file, `required`
* __options__ _(Hash)_ - options for parsing, `{}`
* * __:views__ _(String)_ - directory with templates, `"./views"`
* * __:scope__ _(Object)_ - scope for variables, `self`
* * __:locals__ _(Hash)_ - additional local variables, `{}`
* __locals__ _(Hash)_ - same as above, `{}`

------------------

## How it works ##

### data-var ###

Template
```
#!html
<span data-var="foo"></span>
```

Variable in code
```
#!ruby
foo = "bar"
```

Result
```
#!html
<span>bar</span>
```

### data-attrs ###

Template
```
#!html
<input type="text" data-attrs="name:input.name;value:input.value" />
```

Variable in code
```
#!ruby
input = Object.new
input.name = "field"
input.value = "example"
```

Result
```
#!html
<input type="text" name="field" value="example" />
```

### data-when ###

Template
```
#!html
<div data-when="div">...</div>
```

Variable in code
```
#!ruby
div = false
```

Result
```
#!html

```

__...or...__

Variable in code
```
#!ruby
div = true
```

Result
```
#!html
<div>...</div>
```

### data-when-not ###

Template
```
#!html
<div data-when-not="div">...</div>
```

Variable in code
```
#!ruby
div = false
```

Result
```
#!html
<div>...</div>
```

__...or...__

Variable in code
```
#!ruby
div = true
```

Result
```
#!html

```
### data-each ###

Template
```
#!html
<tr data-each="@files">
  <td data-var="name"></td>
  <td data-var="size"></td>
</tr>
```

Variable in code
```
#!ruby
@files = [{"name" => "first file", "size" => "15Kb"},
          {"name" => "second file", "size" => "20Mb"}]
```

Result
```
#!html
<tr>
  <td>first file</td>
  <td>15Kb</td>
</tr>
<tr>
  <td>second file</td>
  <td>20Mb</td>
</tr>
```


--------------------

## External links ##

* E-mail: [alex.wayfer@gmail.com](mailto:alex.wayfer@gmail.com)
* [RubyGems.org](https://rubygems.org/gems/datagnan)