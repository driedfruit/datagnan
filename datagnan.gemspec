Gem::Specification.new do |s|
	s.name			= 'datagnan'
	s.version		= '1.2.5'
	s.date			= Date.today.to_s
	s.summary		= "HTML DOM-based template engine"
	s.description	= "HTML-templates engine, without specific (often ugly) tags. Based on 'data-' attributes."
	s.authors		= ["Alexander Popov", "driedfruit"]
	s.email			= "alex.wayfer@gmail.com"
	s.files			= ["lib/datagnan.rb"]
	s.homepage		= "https://bitbucket.org/AlexWayfer/datagnan"
	s.license		= "MIT"

	s.add_runtime_dependency 'oga', '~> 0'
end
